class Solution {
    fun dailyTemperatures(temperatures: IntArray): IntArray {
        val size = temperatures.size
        var answer : IntArray = IntArray(size) { 0 }
        var stack = Stack<Int>();
        for(i in 0..(size-1)) {
            while(!stack.isEmpty() && temperatures[i] > temperatures[stack.peek()]) {
                answer[stack.peek()] = i-stack.pop()
            }
            stack.push(i)
        }

        return answer
    }
}